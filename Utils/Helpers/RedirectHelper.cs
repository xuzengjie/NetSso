﻿using System;
using System.Text;
using System.Web.Mvc;

namespace Utils.Helpers
{
    public abstract class RedirectHelper : Controller
    {
        private string GetUrl(string firstTrustedSite, string sourceUrl, string token, string trustedSites = null)
        {
            var url = new StringBuilder($"{firstTrustedSite}?token={token}&sourceUrl={sourceUrl}");
            if (!string.IsNullOrEmpty(trustedSites))
                url.Append($"&trustedSites={trustedSites}");

            return url.ToString();
        }

        /// <summary>
        /// 将token传递至受信任的站点[trustedSites]
        /// </summary>
        /// <param name="sourceUrl">当前网站地址</param>
        /// <param name="token"></param>
        /// <param name="trustedSites">受信任的站点</param>
        /// <returns></returns>
        public RedirectResult RedirectNotice(string sourceUrl, string token, string trustedSites)
        {
            if (string.IsNullOrEmpty(sourceUrl))
                throw new ArgumentNullException(nameof(sourceUrl));

            if (string.IsNullOrEmpty(trustedSites))
                return Redirect(sourceUrl);

            var currentSite = string.Empty; //当前要请求的站点(下一站点)
            var surplusSites = string.Empty; //除去currentSite剩余的站点
            if (!trustedSites.Contains(","))
                currentSite = trustedSites;
            else 
            {
                var tigIdx = trustedSites.IndexOf(',');
                currentSite = trustedSites.Substring(0, tigIdx);
                surplusSites = trustedSites.Substring(tigIdx + 1);
            }

            //一个站点只通知下一个站点，并将剩余的站点列表交至下一站点，如此形成一个闭环
            try 
            {
                var url = GetUrl(currentSite, sourceUrl, token, surplusSites);
                return Redirect(url);
            } catch (Exception) 
            {
                //如果已传递至最后一个时失败了，则直接返回sourceSite
                //如果中途有意站点失效了，则token传递职责交予下下站点执行
                var isEndSite = !trustedSites.Contains(",");
                return isEndSite ? Redirect(sourceUrl) : RedirectNotice(sourceUrl, token, surplusSites);
            }
        }
    }
}
