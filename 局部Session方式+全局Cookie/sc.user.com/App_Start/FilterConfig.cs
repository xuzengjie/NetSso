﻿using sc.user.com.Attributes;
using System.Web.Mvc;

namespace sc.user.com
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CheckLoginAttribute());
        }
    }
}
