﻿using System.Web.Mvc;
using Utils.Commons;

namespace sc.user.com.Attributes
{
    public class CheckLoginAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            //如果是请求登录页面则跳过登录检查
            var url = filterContext.HttpContext.Request.Url?.ToString();
            var sessionToken = filterContext.HttpContext.Session[StaticString.Token]?.ToString();
            var cookieToken = filterContext.HttpContext.Request.Cookies[StaticString.Token];

            if ((!string.IsNullOrEmpty(url) && !url.Contains("LoginAuth")) &&
                (string.IsNullOrEmpty(sessionToken) || string.IsNullOrWhiteSpace(cookieToken?.Value) || sessionToken != cookieToken?.Value)) 
            {
                filterContext.Result = new RedirectResult("~/LoginAuth/Index");
            }
        }   
    }
}